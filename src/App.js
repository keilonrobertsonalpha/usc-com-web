import './App.css';
import Login from './Login.js';
import SignUp from './SignUp.js';
import News from './News.js';
import Notices from "./Notices.js";
import Resources from './Resources.js';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import React from 'react';
import { Switch } from 'react-router';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from "./Header"


function App() {
  return (
    <div>
        <Router>
        <Switch>
        <Route exact path ="/">
            <Login/>
          </Route>
          <Route exact path ="/SignUp" >
            <SignUp/>
          </Route>

          <div>
          <Header/>
          </div>
        </Switch>
        </Router>
    </div>
   
  );
}

export default App;
