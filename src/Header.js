import './App.css';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Switch } from 'react-router';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import NotificationsIcon from '@material-ui/icons/Notifications';
import NewReleasesIcon from '@material-ui/icons/NewReleases';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { ButtonGroup, Hidden } from '@material-ui/core';
import { Title } from '@material-ui/icons';
import { render } from 'react-dom';
import News from './News.js';
import Notices from './Notices.js';
import Resources from './Resources';
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  roots: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },

  root: {
    display: 'flex',
  },

  drawerPaper: {
    width: drawerWidth,

  },

  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },

  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  
  drawerContainer: {
    overflow: 'auto',
  },
  
 

}));


export default function Header() {
    const classes = useStyles();

    return(
        <div>  
        <div className={classes.root}>
        <CssBaseline />
                <AppBar position="fixed" className={classes.appBar}>
                <div className={classes.roots}>
                <AppBar position="static" style={{ background: '#1a7212' }} >
                    <Toolbar >
                    <Typography variant="h6" className={classes.title}>
                        USC COM
                    </Typography>

                    </Toolbar>
                </AppBar>
                </div>
                </AppBar>
                <Drawer
                    className={classes.drawer}
                    variant="permanent"
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                    
                    anchor="left"
                >
                    
                    <div className={classes.toolbar} style={{ background: '#1a7212', }}/>
                
                    
                    <Divider />
                    <List>
                    <ButtonGroup className="MenuButtons"
                    orientation="vertical"
                    aria-label="vertical contained primary button group"
                    variant="none"
                    fullWidth="true"
                    
                >
                    <Button startIcon={<NewReleasesIcon />} style={{ color: '#Dbb725' }} href="/News" > <h4>News</h4></Button>
                    <Button startIcon={<NotificationsIcon />} style={{ color: '#Dbb725' }} href="/Notices"><h4>Notices</h4></Button>
                    <Button startIcon={<PictureAsPdfIcon/>} style={{ color: '#Dbb725' }} href="/Resources"><h4>Resources</h4></Button>
                    <Button startIcon={<ExitToAppIcon />} style={{ color: '#Dbb725' }} href="/" ><h4>Log out</h4></Button>

                </ButtonGroup>
                    </List>
                </Drawer>
                
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Route exact path = "/News">
                        <News/>
                      </Route>
                      <Route exact path = "/Notices">
                        <Notices/>
                      </Route>
                      <Route exact path = "/Resources">
                        <Resources/>
                      </Route>
                
                </main>
        </div>
    </div> 

           
    );
    
}