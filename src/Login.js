import logo from './logo.svg';
import theme from './theme';
import * as firebase from 'firebase';
import SignUp from './SignUp.js';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { FormControl, InputLabel, Input, FormHelperText, TextField, Button, Grid, Paper } from '@material-ui/core';

//const auth = firebase.auth();

//const whenSignedIn = document.getElementById('whenSignedIn');
//const whenSignedOut = document.getElementById('whenSignedOut');

//const SignInbtn = document.getElementById('SignInbtn');
//const SignOutbtn = document.getElementById('SignOutbtn');

//const UserDetails = document.getElementById('UserDetails');

//SignOutbtn.onClick = () => auth.signOut();
function Login() {

  return (
    <div className="background">
      <div className="Login" >
      
      <Grid item xs={12} container justify = "center" alignItems = "center">
        <Paper className={"paper"}>
          <header>
           <h1>Login</h1>
           </header>
                  <div>
                    <form>
                    <Grid container justify = "center">
                      <div className="container">
                      <FormControl  fullWidth="true" size="medium">
                              <InputLabel>USC email</InputLabel>
                              <Input type="email"/>
                            </FormControl>

                            <FormControl fullWidth="true" size="medium">
                              <InputLabel>Password</InputLabel>
                              <Input type="password"/>
                            </FormControl>

                            <div className={"loginbotton"}>
                              <Button className="Lbotton" variant="contained" color="secondary" href="/News" style={{ background: '#1a7212' }}>
                                Login
                              </Button>
                              <Link style={{ color: '#1a7212' }}className="link" to= "/SignUp">Create account</Link>
                            </div>
                       </div>   
                      </Grid>
                  </form> 
                </div>
              
            </Paper>
        </Grid>  
    </div>
      
    </div>   

  );
}

export default Login;