import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import clsx from "clsx";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import DeleteIcon from "@material-ui/icons/Delete";
import Popover from "@material-ui/core/Popover";
import {
  Button,
  ButtonGroup,
  Divider,
  FormControl,
  Grid,
  Input,
  InputLabel,
  List,
  Paper,
  TextField,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  approot: {
    maxWidth: 421,
    flexGrow: 3,
  },

  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },

  formroot: {
    "& .MuiTextField-root": {
      width: "30ch",
    },
  },

  deleteroot: {
    float: "right",
  },

  input: {
    display: "",
    paddingTop: 0,
  },

  typography: {
    padding: theme.spacing(2),
  },
}));

function Notices() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div className="Container">
      <div className="appcontainer">
        <Grid container justify="center" item xs={12} alignItems="center">
          <div className={classes.approot}>
            <AppBar position="static" style={{ background: "#FFFFFF" }}>
              <Toolbar>
                <Typography
                  variant="h6"
                  className={classes.title}
                  style={{ color: "#1a7212" }}
                >
                  <IconButton
                    className="expandbutton"
                    className={clsx(classes.expand, {
                      [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                  Create a post !
                  <Collapse in={expanded} timeout="auto" unmountOnExit>
                    <CardContent>
                      <div>
                        <form
                          className={classes.formroot}
                          noValidate
                          autoComplete="off"
                        >
                          <div className="box">
                            <Grid container justify="center">
                              <div className="appcontainer">
                                <div className={classes.Uploadimage}>
                                  <label htmlFor="icon-button-file">
                                    <IconButton
                                      color="primary"
                                      aria-label="upload picture"
                                      component="span"
                                    >
                                      <PhotoCamera />
                                      <input
                                        accept="image/*"
                                        className={classes.input}
                                        id="icon-button-file"
                                        type="file"
                                      />
                                      <output for="file"></output>
                                    </IconButton>
                                  </label>
                                </div>
                                <div>
                                  <FormControl fullWidth="true" size="medium">
                                    <FormControl fullWidth="true" size="medium">
                                      <TextField
                                        id="outlined-multiline-static"
                                        multiline
                                        rows={4}
                                        label="Post something"
                                        variant="outlined"
                                      />
                                    </FormControl>
                                  </FormControl>
                                </div>
                                <div className={"loginbotton"}>
                                  <Button
                                    className="Sbotton"
                                    variant="contained"
                                    type="submit"
                                    style={{ background: "#1a7212" }}
                                  >
                                    Post!
                                  </Button>
                                </div>
                              </div>
                            </Grid>
                          </div>
                        </form>
                      </div>
                    </CardContent>
                  </Collapse>
                </Typography>
              </Toolbar>
            </AppBar>
          </div>
        </Grid>
      </div>

      <Grid container justify="center" alignItems="center">
        <Card className="root">
          <div className={classes.deleteroot}>
            <IconButton aria-label="delete" onClick={handleClick}>
              <DeleteIcon />
            </IconButton>
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center",
              }}
            >
              <Typography className={classes.typography}>
                Are you sure you want to delete this post?
              </Typography>
              <div className="deletepost">
                <List>
                  <ButtonGroup
                    className="Deletepost"
                    orientation="Horizontal"
                    aria-label="Horizontal contained primary button group"
                    variant="none"
                    fullWidth="true"
                    onClick={handleClose}
                  >
                    <Button style={{ color: "#Dbb725" }}> Confirm</Button>
                    <Button style={{ color: "#Dbb725" }}>Cancel</Button>
                  </ButtonGroup>
                </List>
              </div>
            </Popover>
          </div>
          <CardHeader
            avatar={
              <Avatar
                alt="Profile"
                src="https://upload.wikimedia.org/wikipedia/en/6/6f/Logo_of_the_University_of_the_Southern_Caribbean.png"
              />
            }
            title="Username"
            subheader="April 4, 2021"
          />

          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
              There will be a meeting for comp sci student today at 3:00pm
            </Typography>
          </CardContent>

          <CardActions disableSpacing></CardActions>
          <CardMedia
            className="media"
            image="https://newevolutiondesigns.com/images/freebies/cool-wallpaper-1.jpg"
            style={{ height: 250, width: "100%", objectFit: "cover" }}
          />
        </Card>
      </Grid>
    </div>
  );
}

export default Notices;
