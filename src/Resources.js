import {
  Button,
  Container,
  Grid,
  ListItemText,
  Paper,
  ButtonGroup,
} from "@material-ui/core";
import React from "react";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import clsx from "clsx";
import { DropzoneArea } from "material-ui-dropzone";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import Popover from "@material-ui/core/Popover";
import DownloadLink from "react-download-link";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      maxWidth: 752,
    },

    demo: {
      backgroundColor: theme.palette.background.paper,
    },

    title: {
      margin: theme.spacing(4, 0, 2),
    },

    buttonroot: {
      "& > *": {
        margin: theme.spacing(1),
        float: "right",
      },
    },

    approot: {
      maxWidth: 1075,
      flexGrow: 3,
      marginBottom: 10,
    },

    previewChip: {
      minWidth: 150,
      maxWidth: 1050,
    },

    droproot: {
      flexGrow: 3,
      maxWidth: 1045,
    },

    DropzoneArea: {
      maxWidth: 1045,
    },

    typography: {
      padding: theme.spacing(2),
    },

    nested: {
      paddingLeft: theme.spacing(4),
    },

    deleteroot: {
      float: "right",
    },

    expandOpen: {
      transform: "rotate(180deg)",
    },
  })
);

function generate(element) {
  return [0, 1, 2].map((value) =>
    React.cloneElement(element, {
      key: value,
    })
  );
}

function Resources() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  //upload
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //File collaps
  const [open1, setOpen] = React.useState(true);

  const handleClick1 = () => {
    setOpen(!open1);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div className="Container">
      <Grid item xs={12} container justify="center" alignItems="center">
        <div className={classes.approot}>
          <AppBar position="static" style={{ background: "#FFFFFF" }}>
            <Toolbar>
              <Typography
                variant="h6"
                className={classes.title}
                style={{ color: "#1a7212" }}
              >
                <IconButton
                  className="expandbutton"
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  aria-label="show more"
                >
                  <ExpandMoreIcon />
                </IconButton>
                Upload a file
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                  <div className="container">
                    <DropzoneArea
                      showPreviews={true}
                      showPreviewsInDropzone={false}
                      useChipsForPreview
                      previewGridProps={{
                        container: {
                          spacing: 1,
                          direction: "row",
                          justify: "center",
                          alignItems: "center",
                        },
                      }}
                      previewChipProps={{
                        classes: { droproot: classes.previewChip },
                      }}
                      previewText="Selected files"
                    />
                    <div className={"loginbotton"}>
                      <Button
                        className="Sbotton"
                        type="submit"
                        variant="contained"
                        style={{ background: "#1a7212" }}
                      >
                        Upload
                      </Button>
                    </div>
                  </div>
                </Collapse>
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
        <Paper className={"Rpaper"}>
          <div className="container">
            <Container fixed>
              <div className={"demo"}>
                <List
                  component="nav"
                  aria-labelledby="nested-list-subheader"
                  subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                      <h1>Resources</h1>
                    </ListSubheader>
                  }
                  className={classes.root}
                >
                  <ListItem button onClick={handleClick1}>
                    <ListItemText primary="School of Science and Technology" />
                    {open1 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>

                  <Collapse in={open1} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      <ListItem>
                        <div className={classes.deleteroot}>
                          <IconButton aria-label="delete" onClick={handleClick}>
                            <DeleteIcon />
                          </IconButton>
                          <Popover
                            id={id}
                            open={open}
                            anchorEl={anchorEl}
                            onClose={handleClose}
                            anchorOrigin={{
                              vertical: "bottom",
                              horizontal: "center",
                            }}
                            transformOrigin={{
                              vertical: "top",
                              horizontal: "center",
                            }}
                          >
                            <Typography className={classes.typography}>
                              Are you sure you want to delete this file?
                            </Typography>
                            <div className="deletepost">
                              <List>
                                <ButtonGroup
                                  className="Deletepost"
                                  orientation="Horizontal"
                                  aria-label="Horizontal contained primary button group"
                                  variant="none"
                                  fullWidth="true"
                                  onClick={handleClose}
                                >
                                  <Button style={{ color: "#Dbb725" }}>
                                    {" "}
                                    Confirm
                                  </Button>
                                  <Button style={{ color: "#Dbb725" }}>
                                    Cancel
                                  </Button>
                                </ButtonGroup>
                              </List>
                            </div>
                          </Popover>
                        </div>

                        <DownloadLink
                          label="Student file"
                          filename="packets2.pdf"
                          exportFile={() =>
                            Promise.resolve("cached data here …")
                          }
                        />
                      </ListItem>
                    </List>
                  </Collapse>
                </List>
              </div>
            </Container>
          </div>
        </Paper>
      </Grid>
    </div>
  );
}

export default Resources;
