//import './SignUp.css';
import theme from './theme.js';
import { FormControl, InputLabel, Input, FormHelperText, TextField, Grid, Paper, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

function SignUp () {
  return (
    <div className="background">
    <div className="SignUp">
      <Grid item xs={12} container justify="center" alignItems="center">
        <Paper className={"paper"}>
          <header>
            <h1>Sign Up</h1>
          </header>
          <body>
            <div>
              <form>
                <div className="box">
                  <Grid container justify="center">
                    <div className="container">
                      <div>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>First Name</InputLabel>
                          <Input type="text" />
                        </FormControl>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>Last Name</InputLabel>
                          <Input type="text" />
                        </FormControl>
                      </div>
                      <div>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>USC ID</InputLabel>
                          <Input type="id" />
                        </FormControl>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>USC email</InputLabel>
                          <Input type="email" />
                        </FormControl>
                      </div>
                      <div>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>Password</InputLabel>
                          <Input type="password" />
                        </FormControl>
                        <FormControl fullWidth="true" size="medium">
                          <InputLabel>Password Confirmation</InputLabel>
                          <Input type="password-confirm" />
                        </FormControl>
                      </div>
                      <div className={"loginbotton"}>
                              <Button className="Sbotton" variant="contained" type="submit" style={{ background: '#1a7212' }} >
                                Sign up
                              </Button>
                              <Link style={{ color: '#1a7212' }}className="link" to= "/">Already have an account?</Link>

                            </div>
                    </div>
                  </Grid>
                </div>
              </form>
            </div>
          </body>
        </Paper>
      </Grid>
    </div>
    </div>
  );
}

export default SignUp;